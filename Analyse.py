def A_objet_categorie(E_mot,E_l_recon): #categorie d'un mot voiture -> vehicule
    for k in E_l_recon.keys():
        for i in E_l_recon[k]:
            if i.count(E_mot)>=1:
                return k
    return "fail"

#Ici on se fiche du type precis du mot se lever tot ->pronon(se) ou adj(tot) car en pratique il sert au traitement de fonctions qui traiteront les deux qu'importe l'ordre de toute facon
def A_objet_type(E_mot,E_l_type): #categorie d'un mot composite ex: se lever -> pronominal 
    v_type_objet="normal" #valeur par defaut -> 'neutre'/classique quoi
    v_resT=False
    v_motss=E_mot.split(' ') #on casse le mot composite
    for k in E_l_type.keys(): #cle des types pronom,adj,...
        for i in E_l_type[k] : #mots qui justifie le type me/te,... -> pronon
            for m in v_motss: #on parcourt chaque mot qui constitue le mot
                if (i==m) and ( not k=="normal" ): #si on a un mot special on recupere le type qui correspond
                    v_type_objet=k #stock la valeur
                    v_resT=True
                if k=="normal":
                    v_resT=True
    if v_resT==False :
        print("Erreur st_type aucun reference aux particules comstituant : "+str(E_mot))
        print("Default normal")
    return v_type_objet #retour

#L'idee est de pouvoir indexe les mots dans la base de donne :
# se lever -> lever puis avoir reconnu 'se' plus tard par exemple pour la position de l'agent/patient inverse par le pronom
# ne pas faire dans la bbd -lever :{attribut}
#                          -se lever:{},te lever,... x tout les mots
#                          surtout qu'on a deja extrait le se dans le programme de semantique on peut directement l'utiliser
def A_objet_coeur(E_mot,E_l_type,E_l_coeur): #sujet principal d'un mot composite ex:bus scolaire -> scolaire \  vite fait -> vite
    v_coeur=list(E_mot.split(' ')) #casse les composites
    v_motcopy=E_mot #copy
    v_stop=len(v_coeur)
    while (len(v_coeur)>1) and (v_stop>-1):#on veut le mot principal -> il n'en reste qu'un, les verbes se xxxx jamais pas seuls se g�rent � l'ext�rieur du meme principe que tout les autres et prennent la meme place dans le bbd
        v_mot_type=A_objet_type(v_motcopy,E_l_type) #type du mot pour traitement par exemple pronom, il est avant le mot donc le mot avant sort
        for k in E_l_coeur.keys() : #positions du mot prnon->avant->avant le mot principal
            for i in E_l_coeur[k]: #les types qui correspondent a la position pronom -> avant
                if v_mot_type==i: #type reconnu
                    if i in ["adj"]: #liste des cas indifferenciables complement avant mais aussi apres 
                        v_trouve_normal=False #booleen si je trouve la ou se mot n'est pas
                        v_position=0 #position courante pour parcourt et incrementer la bonne variable position
                        v_position_adj=0 #variable position du mot � degager
                        v_position_normal=0 #variable position a ne pas toucher
                        for m in v_coeur: #parcourt compos�es du mot
                            v_tp=A_objet_type(m,E_l_type) #type du mot courant analyse
                            if v_tp in ["adj"]: #on verifie que c le mot a probleme
                                v_position_adj=v_position #sa position
                            if v_tp=="normal": #on verifie qu'il faut pas le toucher
                                v_trouve_normal=True #bloque le touchage de ce mot donc on indique que tout le reste peut degager
                            if v_trouve_normal is False: #pas sur de ce qu'il faut proteger mais c pas le courant ni avant donc on avance
                                v_position_normal+=v_position_normal+1 #increment la position dans la liste des mots a pas toucher
                            v_position=v_position+1 #on avance le parcourt
                        if v_position_adj>v_position_normal:
                            #je suis apres le normal, je degage ce qui est apres, si c pas lui, de toutes facon il sortira tot ou tard vu qu'il n'est pas bon et celui a probleme degagera tot ou tard dans la boucle
                            v_coeur.pop(len(v_coeur)-1) #degage le mauvais mot a la fin
                        else:
                            v_coeur.pop(0)       #pareil mais avant
                    else :
                        #statistiquement on aurra pas adj adj ,... adj des mots qui vont partous empil�es a font donc ces des mots sans doute et on va un peu plus vite
                        if k=="avant":
                            v_coeur.pop(len(v_coeur)-1)
                        if k=="apres":
                            v_coeur.pop(0)
                    if len(v_coeur)<=1: #j'ai fini alors stop et renvoie
                        return v_coeur
                    v_motcopy=""
                    for m in v_coeur: #mise a jour des mots qui sont sortis
                        v_motcopy+=m
                        v_motcopy+=" "
        v_stop-=1
    if (v_stop<=-1) and (len(v_coeur)>1):
        print("Erreur dans objet_coeur reference fct_coeur de : "+str(E_mot))
    return v_coeur #par defaut tout

#Ici optimiser plutot que d'avoir rapide,veloce,...+attributs dans la bbd, on remarque qu'il vont avec le vitesse ou autres alors on les regroupent dans des mots de regroupement -> ici on utilise dans mots en -ement
# par tout prendre, prendre ceux qui peuvent aller avec une reference mot(a identifier aussi) ,les autres on s'en fiche 
#exc pour -identifier type de mot 0 -> rapide -> adj
#         -mot en brut 1 -> voiture ->jamais utiliser c pour completer au cas ou          
def A_super_mot(E_exc,E_mot,E_adj,E_l_group): #super classe d'un mot
    v_res=[] #resultats possibles
    v_resCat=False
    v_resV=False
    v_resN=False
    if E_exc==0:
        v_categorie=[]
        for k in E_l_group["adjectif"].keys(): #repere adj
            for i in E_l_group["adjectif"][k]:
                if E_adj in i:
                    v_categorie.append(k)
                    v_resCat=True
        for c in v_categorie : 
            for k in E_l_group["verbe"].keys(): #repere si il est utilisable avec le mot  
                for i in E_l_group["verbe"][k]:
                    if E_mot in i:
                        if v_categorie.count(k)>0: #corelation 
                            v_res.append(k)
                            v_resV=True
        if v_resCat==False:
            print("Dans r_group cle[adjectif] l'adj : "+str(E_adj)+" n'a pas trouve de correspondance")
        else:
            if v_resV==False:
                print("Dans r_group cle[verbe] le verbe : "+str(E_mot)+" n'a pas trouve de correspondance")
                
    else:
        for k in E_l_group["NN"].keys():
            for i in E_l_group["NN"][k]:
                if E_mot in i:
                    v_res.append(k)
                    v_resN=True
        if v_resN==False:
            print("Dans r_group cle[NN] le mot : "+str(E_mot)+" n'a pas trouve de correspondance")       
    return v_res

def A_polarite_mot(E_mot_type,E_obj_type,E_l_pola): #point de vue mot si ce lui qui fait l'action est avant ou apres par exemple pronon sa switch
    v_polarite="normal" #defaut
    for k in E_l_pola[E_mot_type].keys():
        for i in E_l_pola[E_mot_type][k]:
            if E_obj_type in i:
                v_polarite=k
    return v_polarite

def A_carac_mot(E_mot,E_l_carac): #caracteristiques associe au mot par exemple depasser->vitesse,poids,...
    v_carac=[]
    for k in E_l_carac.keys():
        for i in E_l_carac[k]:
            if E_mot in i:
                v_carac.append(k)
    if len(v_carac)<=0 :
        print("Dans r_carac le mot : "+str(E_mot)+" n'a pas trouve de correspondance")
    return v_carac

def A_contrainte_mot(E_carac,E_coeur,E_l_contr): #contraintes d'un mot ->plutot pour etre avec le verbe mais les autres ok mais si ca sert a rien
    v_res="rien"
    for k in E_l_contr.keys():
        for i in E_l_contr[k]:
            if E_coeur==i:
                v_data=E_l_contr[k][i]
                for it in v_data :
                    if E_carac.count(it)>0 :
                        v_res=k
    if v_res=="rien" :
        print("Dans r_hierachie le mot de coeur : "+str(E_coeur)+" de caracteristique : "+str(E_carac)+" n'a pas trouve correspondance")
        print("Default superiorite")
        v_res="superiorite"
    return v_res #defaut

def A_condition(E_contrainte): #convertir le format contrainte -> format cle bbd correspondadnte
    if E_contrainte.count("superiorite")>0:
        return "a>pt"
    if E_contrainte.count("inferiorite")>0:
        return "a<pt"
    return "a=pt"
            
