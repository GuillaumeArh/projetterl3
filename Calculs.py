 #connaitre si l'information existe et puisqu'on la, autant garder sa position relative a la bbd
 #sinon dans quel cas de proble me on est
 # 2 c bon
 # 1 il y a tout faut pour ce mot c bete bon ben valeur par defaut\reference,...
 #
 # -2 rien du tout
 # -1 pareil mais on a cherche
 # 0 reference/moyenne pout tout au moins on a une valeur pas random
def Cal_indice_dico_val(E_mot,E_mot_super,E_mot_coeur,E_l_val):
    v_taille=len(E_l_val.keys())
    v_cpt=0
    v_val=-2
    if E_mot_super not in E_l_val.keys():
        v_val=-1
    else:
        for k in E_l_val.keys():
            if E_mot["type"] not in E_l_val[k].keys():
                if v_cpt>=v_taille :
                    return 0
            else :
                if E_mot_coeur not in E_l_val[k][E_mot["type"]]:
                    if v_cpt>=v_taille :
                        return 1
                else :
                    return 2
            v_cpt+=1
    return v_val

def Cal_val_mot(E_indice,E_mot,E_mot_super,E_mot_coeur,E_l_val): #valeur du mot selon le type de probleme si il y en a -> cas repere avec la fonction d'indice precedante
    if E_indice==-1:
        return 0
    if E_indice==0:
        return float(E_l_val[E_mot_super]["other"]["ref"])
    if E_indice==1:
        return float(E_l_val[E_mot_super][E_mot["type"]]["ref"])
    if E_indice==2:
        return float(E_l_val[E_mot_super][E_mot["type"]][E_mot_coeur])
    else:
        return 0

def Cal_val_mot_plus(E_mot,E_sup,E_l_val):
    v_val=0.0
    v_Ens=E_mot.split(' ')
    if E_sup in E_l_val["complement"].keys():
        data=E_l_val["complement"][E_sup].keys()
        for m in v_Ens:
            if m in data:
                v_val+=float(E_l_val["complement"][E_sup][m]["ref"])
    return float(v_val)
