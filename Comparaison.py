 #savoir la comparaison effectue >,<,=,? en fonction des points de vue du verbe et du comparatif
 #verbe pronominal agent apres verbe
 #compartif negatif attention le pronom qui correspond sera inverse par rapport au sens du verbe,...
def CP_type_comparaison(E_pola_v,E_pola_adv,E_l_compar):
    if (len(E_l_compar)<=0) or (E_pola_v is None) or (E_pola_adv is None):
        return "?"
    if E_pola_v not in E_l_compar.keys() :
        print("Dans fct_pola : "+str(E_pola_v)+" n'existe pas")
    if E_pola_adv not in E_l_compar["normal"].keys() :
        print("Dans fct_pola : "+str(E_pola_adv)+" n'existe pas")
    return E_l_compar[E_pola_v][E_pola_adv]

def CP_cond_verification(E_val_ag,E_val_pt,E_cond): #verifie si la condition/contrainte verbe est respecte
    if (E_val_ag==0) and (E_val_pt==0):
        print("Dans r_val bbd incomplete")
        print("Default ok")
        return True
    if E_cond.count("a>pt")>0:
        if E_val_ag<E_val_pt:
            return False
    if E_cond.count("a<pt")>0:
        if E_val_ag>E_val_pt:
            return False
    if E_cond.count("a=pt")>0:
        if not E_val_ag==E_val_pt:
            return False
    return True

 #savoir la comparaison effectue >,<,=,? en fonction des points de vue du verbe et du comparatif
 #verbe pronominal agent apres verbe
 #compartif negatif attention le pronom qui correspond sera inverse par rapport au sens du verbe,...
 #verifie si le rapport enonce de base n'est pas idiot en en utilisant une approche par chifffre=cas , plutot qu lecture direct bbd
 #---> en gros pour etre sur
def CP_rapport_final(E_rapport_init,E_polarite_v,E_polarite_adv): #pdv
    if (E_rapport_init.count("=")>0) or (E_rapport_init.count("?")>0):
        return E_rapport_init
    else :
        v_inv=0
        if E_polarite_v.count("inverse")>0 :
            v_inv+=1
        if E_polarite_adv.count("negatif")>0 :
            v_inv+=1
        if not v_inv==1 :
            return E_rapport_init
        else :
            if E_rapport_init.count(">")>0:
                return "<"
            if E_rapport_init.count("<")>0:
                return ">"
        return
            
def CP_hierachie(E_agent,E_patient,E_val_ag,E_val_pt,E_cond): #Avec les valeurs associe, qui est le vrai agent/patient par rapport a la contrainte impose
    print("-------Begin analyse--------")
    if E_cond.count("superiorite"):
        if E_val_ag>E_val_pt:
            v_sup=E_agent["mot"]
            v_inf=E_patient["mot"]
        else :
            v_sup=E_patient["mot"]
            v_inf=E_agent["mot"]
    else :
        if E_val_ag>E_val_pt:
            v_sup=E_patient["mot"]
            v_inf=E_agent["mot"]
        else :
            v_sup=E_agent["mot"]
            v_inf=E_patient["mot"]
    print("--------End----------")
    return {"+":v_sup,"-":v_inf}
