#Test pour la fonction semantique
agt={"mot":"voiture rouge","type":"vehicule","confiance":3}
pat={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel={"verbe":"me depasser vite","adjectif":"rapide","comparaison":"moins","negation":False}

agt2={"mot":"rapide voiture rouge","type":"vehicule","confiance":3}
pat2={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel2={"verbe":"me retarder vite","adjectif":"lent","comparaison":"moins","negation":True}

agt3={"mot":"voiture rouge","type":"vehicule","confiance":3}
pat3={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel3={"verbe":"me depasser vite","adjectif":"rapide","comparaison":"autant","negation":False}

agt4={"mot":"voiture","type":"vehicule","confiance":3}
pat4={"mot":"bus","type":"vehicule","confiance":None}
rel4={"verbe":"depasser","adjectif":"rapide","comparaison":"plus","negation":False}

agt5={"mot":"rapide voiture rouge","type":"vehicule","confiance":3}
pat5={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel5={"verbe":"me ralentir vite","adjectif":"lent","comparaison":"plus","negation":False}

agt6={"mot":"rapide voiture rouge","type":"vehicule","confiance":3}
pat6={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel6={"verbe":"peser","adjectif":"lourd","comparaison":"plus","negation":False}

agt7={"mot":"rapide voiture rouge","type":"vehicule","confiance":3}
pat7={"mot":"bus scolaire","type":"vehicule","confiance":None}
rel7={"verbe":"battre","adjectif":"force","comparaison":"plus","negation":False}

