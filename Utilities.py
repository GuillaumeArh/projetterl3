import json
def UT_lecture_json(E_op,E_rel,E_fichier) : #lecture d'un fichier au format json
    v_file=open(E_fichier,'r+')
    v_json_content=v_file.read()
    v_obj_python=json.loads(v_json_content)
    if E_op==False :
        if E_rel is None :
            print(v_obj_python)
        else :
            print(v_obj_python[E_rel])
        v_file.close()
    else :
        v_file.close()
        return v_obj_python

def UT_affiche(E_res): #affichage semantique
    print("Pour "+"[verbe:"+E_res["verbe"]+"]\\"+"[adjectif:"+E_res["adjectif"]+"]"+"\[comparatif:"+E_res["comparatif"]+"] -> "+E_res["sup"]+" "+"est "+E_res["comparatif"]+" "+E_res["adjectif"]+" "+"que "+E_res["inf"]+"\n")
    return

def UT_all_lecture(): #lecture des fichiers
    v_ig=UT_lecture_json(True,None,"r_group.json") #super classe
    v_ip=UT_lecture_json(True,None,"fct_pola.json") #point de vue de l'objet
    v_icp=UT_lecture_json(True,None,"fct_comp.json") #rapport de comparaison
    v_icr=UT_lecture_json(True,None,"r_carac.json") #caracteristique associe mot
    v_iv=UT_lecture_json(True,None,"r_val.json") #valeur par caracteristique
    v_cm=UT_lecture_json(True,None,"fct_coeur.json") #mot principal -> composites
    v_tm=UT_lecture_json(True,None,"st_type.json") #type de composition du mot composite (verbe -> pronominal,accord cod,...)
    v_h=UT_lecture_json(True,None,"r_hierarchie.json") #type contrainte mot
    v_r=UT_lecture_json(True,None,"st_reconnaissance.json") 
    return {"rec":v_r,"hyper":v_ig,"pdv":v_ip,"comparaison":v_icp,"caracteristique":v_icr,"valeur":v_iv,"coeur":v_cm,"type":v_tm,"contrainte":v_h}

def UT_intersection(E_l1,E_l2): #intersection des element de deux listes
    v_res=[]
    for e in E_l2:
        if e in E_l1:
            v_res.append(e)
    return v_res

    
