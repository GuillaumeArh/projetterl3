

import stanza
import sys
import json
#A faire un première fois puis à commenter
#stanza.download('fr')
from fonctionProg import validationEtRecuperation,appelDirectJDM
#nlp = stanza.Pipeline(lang="fr", processors="tokenize,mwt,pos,lemma,depparse")
#doc = nlp(sys.argv[1])
def mainStanza(nomFichier,nomFichierResultat):
        fichier = open(nomFichier, 'r')
        resultat = open(nomFichierResultat,'w')
        ligne  = (fichier.read())
        agent,patient = [],[]
        dico = {}
        find = False
        compteur = 0
        l_rel= [6,7]
        dictionaryAgt = []
        dictionaryPat = []
        dictionaryRel = []
        for line in ligne.splitlines():   #On parcourt par ligne dans le fichier texte
                if line == '':continue    #Si jamais la ligne est vide
                print('####################################################################')
                print(line)
                recup=validationEtRecuperation(line)  #On test la ligne et on recupere agent/patient,etc
                if recup == False:continue
                matche,agent,patient = [],[],[],

                for rel in l_rel:
                        for mots in appelDirectJDM(recup[0],rel,0):
                                agent.append(mots)    #On recup les infos avec jdm
                        for mots in appelDirectJDM(recup[1], rel, 0):
                                patient.append(mots)
                matche = set(agent) & set(patient)   #On garde que les elements en communs
                print("Voici l'ensembles des liens en communs entre les 2 sujets : ", matche)
                carac,type = '',''

                for potentiel in matche: #On parcourt les caracteristiques des liens en communs qu'on a eu
                        for attribut in appelDirectJDM(potentiel,17,0):
                                if attribut == recup[3]: #on test si une caracteristique correspond a l'adjectif
                                        carac = attribut
                                        type = potentiel
                                        print("Caracteristiques compatibles trouvé ! : ",carac)
                                        find = True
                                        break
                        if find == True: break

                dictionaryAgt = ({"mot" : recup[0],"type":  type}) #On construit le dico a donner au json.
                dictionaryPat = ({"mot": recup[1], "type": type})
                dictionaryRel = ({"verbe": recup[2],"negation" : recup[5], "adjectif": recup[3],"comparaison": recup[4]})

                dico.update({str(compteur) : { "agt" : dictionaryAgt,"pat" : dictionaryPat,"pat" : dictionaryPat,"rel" : dictionaryRel}})
                compteur += 1

        json.dump(dico,resultat,indent=4) #on écrit le json.
        return nomFichierResultat



