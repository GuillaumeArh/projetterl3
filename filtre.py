import re
import unidecode
def Filtre_code(E_mot):
    return str(unidecode.unidecode(E_mot))

def Filtre_esp(E_mot):
    return str(E_mot).strip()

def Filtre_carac(E_mot,E_carac):
    return str(E_mot).replace(E_carac,'')

def Filtre_n(E_mot):
    return str(E_mot).replace('\\n','')

def Filtre(E_mot):
    return Filtre_esp(Filtre_code(E_mot))

def Filtre_rien(E_m):
    v_phrases=[]
    v_res=[]
    for x in E_m:
        v_phrases.append(Filtre_esp(x))
    for x in v_phrases:
        if (x!='\\n') and (x!=''):
            v_res.append(Filtre_esp(x))
    return v_res

GLOBAL_pronoms=["il","Elle"]
GLOBAL_separateurs=[","]
